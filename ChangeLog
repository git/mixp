2021-12-16  Thien-Thi Nguyen  <ttn@gnuvola.org>

	Release: 0.9

	* configure.ac (AC_INIT): Bump version to "0.9" for release.

2021-12-16  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[build] If Expat is not found, signal error

	* configure.ac: Following AM_WITH_EXPAT,
	if shell var ‘EXPAT_LIBS’ is empty, signal error.

2021-12-16  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[build] Use EXPAT_LIBS from configure script in makefile

	* src/Makefile.am (AM_LDFLAGS):
	Don't hardcode ‘-lexpat’; instead, use $(EXPAT_LIBS).

2021-12-16  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[build] Default to system Expat if not given ‘--with-expat’

	* configure.ac (with_expat): Prior to AM_WITH_EXPAT,
	check this shell var; if unset, assign "yes" to it.

2021-12-16  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[build int] Use AM_WITH_EXPAT

	* configure.ac (expat): Delete this AC_ARG_WITH.
	(EXPAT_INSTALLED): Delete this AM_CONDITIONAL.
	(EXPAT_CFLAGS, EXPAT_LIBS): No longer set these vars.
	(expat): Delete this AC_CHECK_LIB.
	(expat.h): Delete this AC_CHECK_HEADERS.
	(AM_WITH_EXPAT): New macro call.

2021-12-16  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[v slog] Accomodate Guile 2.2

	* test/x001: For Guile 2.2, import also
	 ‘(rnrs bytevectors)’, ‘(ice-9 iconv)’.
	(unicode-string): New proc.
	("U:utf8->latin1 on Latin1"): Use ‘unicode-string’.

2020-11-16  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[v] Suppress Guile "auto-compilation".

	* test/runtest.in: Set GUILE_AUTO_COMPILE; export it.

2020-11-08  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[build] Suppress Guile "auto-compilation".

	* configure.ac: Set GUILE_AUTO_COMPILE; export it.

2020-10-22  Thien-Thi Nguyen  <ttn@gnuvola.org>

	Release: 0.8

	* configure.ac (AC_INIT): Bump version to "0.8" for release.

2020-02-05  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[v] Say "entities", not "unparsed entities".

	* test/x001 ("Test doctype, entities, notations"): ...here.
	This does not change he functionality of the test.

2020-02-05  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[doc] Fix typo; avoid camel-case arg names.

	* doc/ref.texh (Expat handlers): For ‘entity-decl’,
	say "notation-name", not "notationName".

2020-02-05  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[utils] Don't bother w/ ‘unparsed-entity-decl’ for ‘xml->tree’.

	* src/utils.scm (xml->tree): Drop
	‘unparsed-entity-decl’ as one of the handlers.

2020-01-28  Thien-Thi Nguyen  <ttn@gnuvola.org>

	Create .lz tarball, not .xz, for "make dist".

	* configure.ac (AM_INIT_AUTOMAKE): Add
	option ‘dist-lzip’; drop option ‘dist-xz’.

2020-01-28  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[build] Don't try to distribute build-aux/{sofix,uninstall-sofixed}.

	Omission from 2020-01-19, "Use Guile-BAUX for sofix, uninstall-sofixed".

	* Makefile.am (EXTRA_DIST): Remove
	build-aux/sofix, build-aux/uninstall-sofixed.

2020-01-27  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[doc] Mention retirement of ‘hget’.

	* doc/ref.texh (Expat handlers): ...here.

2020-01-27  Thien-Thi Nguyen  <ttn@gnuvola.org>

	Use plist for ‘hset!’ throughout.

	* doc/mixp.texi (Sample programs, How to...): ...here.
	* src/simit.scm (form-port): Likewise.
	* src/utils.scm (xml->tree): Likewise.
	* test/x001 (hset!/p): Delete proc;
	throughout, replace it w/ ‘E:hset!’ directly.

2020-01-27  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[expat] Make ‘hset!’ accept plist as well as alist.

	* src/expat.c (hset_x): ...here, and document
	the planned removal of alist support in doc-comment.

2020-01-27  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[expat] New (mixp expat) proc: hget-one

	* src/expat.c (hget_one): New PRIMPROC.
	* doc/ref.texh (Expat handlers): Add @tsin for ‘hget-one’.

2020-01-27  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[doc] Don't mention ‘hset!’ in ‘hget’ doc.

	* src/expat.c (hget): ...here, in doc-comment;
	instead simply say the keys are "handler names".

2020-01-26  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[Scheme int] Rewrite looping construct sans loop vars.

	* src/utils.scm (parse-data next): Delete proc.
	(parse-data): Don't use ‘next’ rv as loop var ‘line’;
	instead, use no loop vars and ‘let’-bind ‘line’ once per loop.

2020-01-23  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[doc] Spell "Antarctica" correctly!

	* doc/ref.texh (Expat handlers): ...here.

2020-01-23  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[doc] Document ‘unparsed-entity-decl’ obsolescence.

	* doc/ref.texh (Expat handlers): ...here.

2020-01-23  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[expat] Add handler: entity-decl

	* src/expat.c (hnames): Add "entity-decl".
	(hindices): Add ‘hx_entity_decl’.
	(generic_entity_decl): New static func.
	(hset_x): Add handling for ‘entity_decl’.
	* test/x001 ("Test doctype, entities, notations"):
	Don't declare/check ‘unparsed-entity-decl’;
	instead, declare/check ‘entity-decl’.
	* doc/ref.texh (Expat handlers): Add @deffn for ‘entity-decl’.

2020-01-23  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[expat] Add handler: skipped-entity

	* src/expat.c (hnames): Add "skipped-entity".
	(hindices): Add ‘hx_skipped_entity’.
	(generic_skipped_entity): New static func.
	(hset_x): Add handling for ‘skipped_entity’.
	* doc/ref.texh (Expat handlers): Add @deffn for ‘skipped-entity’.

2020-01-23  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[expat] Add handlers: doctype-decl-{start,end}

	* src/expat.c (hnames): Add "doctype-decl-start", "doctype-decl-end".
	(hindices): Add ‘hx_doctype_decl_start’, ‘hx_doctype_decl_end’.
	(generic_doctype_decl_start): New static func.
	(generic_doctype_decl_end): Likewise.
	(hset_x): Add handling for ‘doctype_decl_start’, ‘doctype_decl_end’.
	* test/x001 ("Test doctype, entities, notations"):
	Rename from "Test unparsed entities and notations";
	also declare/check ‘doctype-decl-start’, ‘doctype-decl-end’.
	* doc/ref.texh (Expat handlers): Add @deffn:s for
	‘doctype-decl-start’, ‘doctype-decl-end’.

2020-01-23  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[v] Make test accumulate/check all handler args.

	* test/x001 ("Test unparsed entities and notations" more): New proc.
	("Test unparsed entities and notations"): Use ‘more’.

2020-01-23  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[doc] Update "Not implemented" section.

	* doc/ref.texh (Not implemented): Drop ‘XML_SetEncoding’,
	‘XML_StopParser’, ‘XML_ResumeParser’; add ‘XML_ParserCreate_MM’,
	‘XML_MemMalloc’, ‘XML_MemRealloc’, ‘XML_MemFree’.

2020-01-23  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[utils slog] Avoid uniform-vector-* procs for Guile 2.

	* src/utils.scm [guile-2]: Import ‘(rnrs bytevectors)’.
	(uve-length, uve-ref, uve-type): New bifurcated procs/aliases.
	(make-unsigned-byte-stream): Use ‘uve-length’, ‘uve-ref’,
	‘uve-type’; use ‘bytevector?’ for Guile 2.

2020-01-23  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[utils int] Use ‘get-u8’ for Guile 2.

	This change is not marked ‘slog’ because it improves
	efficiency, and does not deal w/ deprecated items.

	* src/utils.scm [guile-2]: Import ‘(rnrs io ports)’.
	(make-unsigned-byte-stream from-port): Bifurcate, w/
	‘guile-2’ branch using ‘get-u8’; otherwise, previous.
	(make-unsigned-byte-stream from-string): New proc.
	(make-unsigned-byte-stream): Use ‘from-string’ for strings,
	bifurcating, w/ ‘guile-2’ branch setting the fluid
	‘%default-port-encoding’ to "ISO-8859-1" around the call.
	* doc/ref.texh (High-level extensions): Mention common ‘from’
	processing, including portability note on Guile 2 handling,
	for ‘utf8->latin1’, ‘utf8->ucs2’, ‘utf8->ucs4’.

2020-01-22  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[expat] New (mixp expat) proc: set-hash-salt

	* src/expat.c (set_hash_salt): New PRIMPROC.
	* test/x001: Import ‘(mixp expat) set-hash-salt’.
	("Test handlers"): Add two test cases for ‘set-hash-salt’.
	* doc/ref.texh (Expat misc): Add @tsin for ‘set-hash-salt’.

2020-01-22  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[expat] New (mixp expat) proc: get-parsing-status

	* src/expat.c (parsing_statuses): New static array of SCM.
	(get_parsing_status): New PRIMPROC.
	(init_gexpat): Initialize ‘parsing_statuses’.
	* test/x001: Import ‘(mixp expat) get-parsing-status’.
	("Test handlers"): Add test case between parsing phases
	to test ‘get-parsing-status’.
	* doc/ref.texh (Doing a parse): Add @tsin for ‘get-parsing-status’.

2020-01-21  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[expat] New (mixp expat) procs: stop-parser, resume-parser

	* src/expat.c (stop_parser, resume_parser): New PRIMPROCs.
	* test/x001: Import (mixp expat) procs ‘stop-parser’, ‘resume-parser’.
	("Test handlers" element-start): When the element is "Chapter 1",
	suspend the parse -- stop it, specifying that it be resumable.
	("Test handlers"): Change status message of first phase
	to "parse (w/ stop)"; add second phase using ‘resume-parser’.
	* doc/ref.texh (Doing a parse):
	Add @tsin for stop-parser, resume-parser.

2020-01-21  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[doc] Escape column-0 open paren in doc comment.

	* src/expat.c (expat_version): ...here.

2020-01-21  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[expat] New (mixp expat) proc: set-encoding

	* src/expat.c (set_encoding): New PRIMPROC.
	* test/x001: Import ‘(mixp expat) set-encoding’.
	("Try to parse a not well-formed document"):
	Use ‘set-encoding’; check its return value.
	* doc/ref.texh (Encodings): Add @tsin for ‘set-encoding’.

2020-01-21  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[expat] New (mixp expat) proc: expat-version

	* src/expat.c (expat_version): New PRIMPROC.
	* test/x001: Import from (ice-9 regex)
	‘string-match’, ‘match:substring’.
	("Test version info"): New test case.
	* doc/ref.texh (Expat interface): Add @tsin for ‘expat-version’.

2020-01-21  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[doc] Specify ‘--forward-scan’ to ‘$(gx) c-tsar’.

	We need this because of ‘PRIMPROC’, sigh.

	* src/Makefile.am (c.snippets): ...here.

2020-01-21  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[v] Only accept "make check" option if its value is "1".

	This is so "make check TRACE=0" (and so forth) DTRT.

	* test/x001 (VERBOSE, MEMCHECK, TRACE): Change from going
	into effect if set, to going into effect if value is "1".
	* test/README: Document env vars.

2020-01-21  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[v int] Make "\n" display as "\\n" in ‘character-data’ trace.

	This doesn't change anything, but it is nicer to look at.

	* test/x001 ("Test handlers"): ...here.

2020-01-21  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[v] Add abstraction: sixty-seven-and-eight-encoding

	* test/x001 (sixty-seven-and-eight-encoding): New proc.
	("Create and print an encoding")
	("Test parsing of unknown encoding"):
	Use ‘sixty-seven-and-eight-encoding’.

2020-01-19  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[build int] Use Guile-BAUX for sofix, uninstall-sofixed.

	* autogen.sh: Also import Guile-BAUX
	program modules sofix, uninstall-sofixed.
	* src/Makefile.am (install-data-hook): Use ‘$(gx) sofix’.
	(uninstall-hook): Use ‘$(gx) uninstall-sofixed’.
	* build-aux/sofix: Delete file.
	* build-aux/uninstall-sofixed: Likewise.

2019-12-25  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[doc] Fix typo: Spell "integration" correctly.

	* doc/ref.texh (Not implemented): ...here.

2019-12-25  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[expat] New (mixp expat) proc: get-attribute-info

	* configure.ac (XML_GetAttributeInfo): New AC_CHECK_LIB.
	(HAVE_GETATTRIBUTEINFO): New AC_DEFINE.
	* src/gi.h (SETCAR): New #define.
	* src/expat.c (XML_ATTR_INFO): New #define, before #include <expat.h>.
	(sym_u64): New static SCM.
	(get_attribute_info): New PRIMPROC.
	(init_gexpat): Initialize ‘sym_u64’.
	* doc/ref.texh (Expat misc): Add @tsin for ‘get-attribute-info’.
	* test/x001: Import ‘(mixp expat) get-attribute-info’.
	("Test get-attribute-info"): New test.

2019-12-25  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[expat] New (mixp expat) proc: get-feature-list

	* src/expat.c (get_feature_list): New PRIMPROC.
	* doc/ref.texh (Expat interface): Add @tsin for ‘get-feature-list’.
	* test/x001: Import ‘(mixp expat) get-feature-list’.
	("Test features"): New test.

2019-12-22  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[expat] Add support for ‘XML_ERROR_INVALID_ARGUMENT’.

	* src/expat.c (error_codes) [0x020201 <= EXPAT_VERSION]:
	Add pair for ‘XML_ERROR_INVALID_ARGUMENT’.
	* doc/ref.texh (Symbols): Update list of symbolic error codes,
	by adding ‘INVALID_ARGUMENT’ and reflowing the two columns.

2019-12-22  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[expat int] Add abstraction: EXPAT_VERSION

	* src/expat.c (EXPAT_VERSION): New #define.
	(error_codes): Use ‘EXPAT_VERSION’.

2019-12-22  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[doc] Document valid CODE values for ‘set-param-entity-parsing’.

	* src/expat.c (set_param_entity_parsing): ...here, in doc comment.

2019-12-22  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[doc] Mention arg type for ‘set-base’.

	* src/expat.c (set_base): ...here, in doc comment.

2019-12-22  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[v int] Add abstraction: err-sym-n-str

	* test/x001 (err-sym-n-str): New proc.
	("Test handlers", "Try to parse a not well-formed document")
	("Test unparsed entities and notations")
	("Use a document with characters which have a wrong encoding")
	("Test parsing of unknown encoding", "Use the base")
	("Test the DTD parsing feature"): Use ‘err-sym-n-str’.

2019-12-21  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[doc] Mention bug-report email address.

	* doc/Makefile.am (mixp_TEXINFOS): Add bugaddr.texi.
	(EXTRA_DIST) [MAINTAINER_MODE]: Add bugaddr.texi.
	(bugaddr.texi) [MAINTAINER_MODE]: New target.
	* doc/mixp.texi <pre-Top>: Include bugaddr.texi.
	(Bugs and suggestions): Use @value{BUGADDR}.

2019-12-21  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[doc] Add detailed node listing.

	* doc/mixp.texi (Top): ...here, extending
	@menu block with a @detailmenu block.

2019-12-21  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[doc] Add @copying, @insertcopying blocks.

	* doc/mixp.texi: Add @copying block prior to Top node.
	(@titlepage): Add @vskip and @insertcopying.

2019-12-21  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[doc] Remove unnecessary article.

	* doc/ref.texh (Encodings): ...here, before "better".

2014-12-30  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[expat slog] Use ‘SCM_VALIDATE_INT_COPY’.

	* src/expat.c (parse_buffer): ...here.

2014-12-30  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[expat int] Use ‘SCM_VALIDATE_PROC’, not ‘SCM_VALIDATE_CLOSURE’.

	* src/expat.c (make_encoding): ...here.

2014-12-30  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[expat int] Use SNUGGLE ‘PROCEDUREP’.

	* src/gi.h (PROCP): New macro.
	* src/expat.c (PROCP): Delete macro.

2014-12-30  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[int] Use SNUGGLE headers.

	* configure.ac: Call ‘SNUGGLE_CHECK_CLASSIC_HEADERS’.
	* src/Makefile.am (expat): Prefix thunk-name template w/ "scm_".
	* src/gi.h: #include "snuggle/level.h", "snuggle/humdrum.h",
	"snuggle/defsmob.h", "snuggle/finangle.h", "snuggle/modsup.h".
	(NOINTS, INTSOK): Use ‘GI_LEVEL_1_8’.
	(GI_LEVEL, GI_LEVEL_NOT_YET_1_8, NULLP, STRINGP, BOOLEAN)
	(NUM_INT, SYMBOL, STRING, BSTRING, C_INT, VECREF, EQ, CONS, CAR)
	(CDR, APPLY, LISTIFY, CALL0, CALL1, CALL2, DEFSMOB, GCMALLOC)
	(GCFREE, GCRV, GH_DEFPROC, GH_MODULE_LINK_FUNC)
	(MOD_INIT_LINK_THUNK): Delete #define:s, macros.
	* src/expat.c (range_t): Delete typedef.
	(RS, RLEN, FINANGLABLE_SCHEME_STRING_FROM_SYMBOL, ROZT_X)
	(_FINANGLE, UNFINANGLE): Delete macros.
	(SCM_VALIDATE_VECTOR_LEN): Use ‘GI_LEVEL_PRECISELY_1_8’.
	(parser_p): Use ‘GI_LEVEL_1_8’.
	(get_locus, JAM): Use ‘VECTOR_REF’.
	(MOD_INIT_LINK_THUNK): De-bifucrate, keeping ‘!LAME_LAME_LAME’
	branch; also, remove spurilous trailing semicolon.

2014-12-29  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[boot] Add SNUGGLE headers.

	* autogen.sh: Invoke "guile-baux-tool snuggle".
	* src/Makefile.am (EXTRA_DIST): Add snuggle.

2014-12-29  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[build int] Use "guile-baux-tool snuggle" for snuggle.m4.

	* build-aux/snuggle.m4: Delete file.
	* autogen.sh: Invoke "guile-baux-tool snuggle".

2014-12-28  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[expat] Fix bug: Use ‘GCMALLOC’ for parser handler vector.

	Regression introduced / omission from 2012-01-25,
	"Add abstractions: GCMALLOC, GCFREE, GCRV".

	* src/expat.c (make_parser): ...here.

2014-12-28  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[v slog] Use first-line ‘coding’ cookie; use value nice to Guile 2.

	* test/x001: Move ‘coding’ cookie from eof "Local variables"
	block to first line; change value to ‘iso-8859-1’.

2013-12-22  Thien-Thi Nguyen  <ttn@gnuvola.org>

	Change bug-report email address.

	* configure.ac (AC_INIT): ...here.

2013-08-29  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[doc slog] Add blank lines around @tsin directives.

	Apparently, makeinfo 5.0 doesn't automagically add blank
	lines around @deffn et al, like makeinfo 4.13 used to do.
	<http://lists.gnu.org/archive/html/bug-texinfo/2013-02/msg00129.html>

	* doc/ref.texh: ...here, throughout.

2013-03-10  Thien-Thi Nguyen  <ttn@gnuvola.org>

	Release: 0.7

	* configure.ac (AC_INIT): Bump version to "0.7" for release.

2013-03-10  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[dist] Create .xz tarball instead of .gz on "make dist".

	* configure.ac (AM_INIT_AUTOMAKE): Add options ‘no-dist-gzip’, ‘dist-xz’.

2013-03-10  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[v int] Make sure "tests" are run serially.

	At the moment, there is only one test.  :-/
	Still, the home-grown test driver is not ready to
	be replaced by Automake parallel tests machinery.

	* configure.ac (AM_INIT_AUTOMAKE): Add option ‘serial-tests’.

2013-03-08  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[boot] Bump Automake min vers to 1.12.2.

	* configure.ac (AM_INIT_AUTOMAKE): ...here.

2012-08-13  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[boot] Handle "non-POSIX archiver" requirement.

	This is prompted by a warning from autoreconf.

	* configure.ac (AM_PROG_AR): New macro call.

2012-01-25  Thien-Thi Nguyen  <ttn@gnuvola.org>

	Release: 0.6

	* configure.ac (AC_INIT): Bump version to "0.6" for release.

2012-01-25  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[dist] Distribute test/REC-xml-20081126.xml.

	Omission from 2011-08-07, "Use 5th edition of REC-xml".

	* test/Makefile.am (EXTRA_DIST): Remove
	REC-xml-19980210.xml; add REC-xml-20081126.xml.

2012-01-25  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[maint] Upgrade snuggle.m4.

	* build-aux/snuggle.m4: Sync w/ upstream serial 5.

2012-01-25  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[slog] Specialize for ‘!GI_LEVEL_NOT_YET_1_8’.

	This eliminates deprecation warnings for "make check",
	which admittedly is not known to be comprehensive.

	* src/gi.h (NULLP, BOOLEAN, NUM_INT, SYMBOL, STRING)
	(BSTRING, C_INT, VECREF, EQ, CONS, CAR, CDR, APPLY, LISTIFY)
	(CALL0, CALL1, CALL2, DEFSMOB, GCMALLOC, GCFREE, GCRV, NOINTS)
	(INTSOK): Bifurcate, adding new elems for ‘!GI_LEVEL_NOT_YET_1_8’.
	(SCM_ROCHARS, SCM_ROLENGTH, SCM_ROUCHARS): Delete macros.
	* src/expat.c (FINANGLABLE_SCHEME_STRING_FROM_SYMBOL)
	(REND, NUL_AT_END_X, _FINANGLE, UNFINANGLE):
	Bifurcate, adding new elems for ‘!GI_LEVEL_NOT_YET_1_8’.
	(make_encoding) [!GI_LEVEL_NOT_YET_1_8]: Create a local
	array handle; use it extract the base; release afterwards.

2012-01-25  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[slog] Re-implement ‘SCM_VALIDATE_VECTOR_LEN’ for Guile 1.8.x.

	This avoids deprecation warnings, sigh.

	* src/expat.c [0x0108 == GI_LEVEL] (SCM_VALIDATE_VECTOR_LEN): #undef
	and re-#define, using ‘scm_is_vector’ and ‘scm_c_vector_length’.

2012-01-25  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[slog] Add abstractions: GI_LEVEL, GI_LEVEL_NOT_YET_1_8

	* gi.h (GI_LEVEL, GI_LEVEL_NOT_YET_1_8): New #define:s.

2012-01-25  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[expat int] Add abstractions: GCMALLOC, GCFREE, GCRV

	* src/gi.h (GCMALLOC, GCFREE, GCRV): New macros.
	* src/expat.c (free_parser): Add local typedef ‘ud_struct_t’;
	use ‘GCFREE’, ‘GCRV’.
	(free_encoding): Use ‘GCFREE’, ‘GCRV’.
	(make_encoding): Use ‘GCMALLOC’.

2012-01-25  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[expat] Don't declare smob sizes.

	This undoes the misguided 2011-08-12, "Fix bug: Declare smob sizes".

	* src/gi.h (DEFSMOB): No longer take ‘sz’.
	* src/expat.c (init_gexpat): Update calls to ‘DEFSMOB’.

2012-01-25  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[expat int] Use ‘NOINTS’, ‘INTSOK’ more.

	* src/expat.c (make_parser, make_encoding): Use ‘NOINTS’, ‘INTSOK’.

2012-01-25  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[expat int] Add abstractions for "symbol/string as C byte range".

	* src/expat.c (range_t): New typedef.
	(RS, RLEN, FINANGLABLE_SCHEME_STRING_FROM_SYMBOL, ROZT_X)
	(_FINANGLE, UNFINANGLE, FINANGLE_RAW, FINANGLE): New macros.
	(generic_external_entity_ref): Use ‘FINANGLE_RAW’, ‘UNFINANGLE’.
	(parser_create, parser_create_ns, set_base):
	Use ‘FINANGLE’, ‘UNFINANGLE’.
	(parse): Use ‘FINANGLE_RAW’, ‘UNFINANGLE’.
	* src/gi.h (ROZT_X, ROZT): Delete macros.

2012-01-25  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[int] Decruft: Delete unused #define:s.

	* src/gi.h (PAIRP, NUMBERP, PROCEDUREP, NUM_LONG, NUM_ULONG)
	(C_BOOL, C_ULONG, VECTOR_LEN, CALL3): Delete #define:s.

2012-01-25  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[int] Use ‘STRINGP’ more.

	* src/gi.h (ASSERT_STRING): Use ‘STRINGP’.

2011-08-22  Thien-Thi Nguyen  <ttn@gnuvola.org>

	New module: (mixp simit)

	* src/simit.scm: New file.
	* src/Makefile.am (schemestems): Add simit.
	* doc/mixp.texi (Top): Add menu item "Imitating SXML".
	* ref/texh.texh (Imitating SXML): New node/chapter.

2011-08-15  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[build int] Decruft: Delete unused LIB{CURRENT,REVISION,AGE} vars.

	* configure.ac (LIBCURRENT, LIBREVISION)
	(LIBAGE): Don't set var; don't AC_SUBST.

2011-08-12  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[build int] Use snuggle.m4 more.

	* build-aux/snuggle.m4: Sync w/ upstream serial 4.
	* configure.ac: Do ‘s/GUILE_PROGS/SNUGGLE_PROGS/’,
	‘s/DETERMINE_GUILE_LIBSITE_DIR/SNUGGLE_GUILE_LIBSITE_DIR/’,
	‘s/GUILE_TOOLS_EXISTSP/SNUGGLE_GUILE_TOOLS_EXISTSP/’,
	‘s/GUILE_FLAGS/SNUGGLE_FLAGS/’.

2011-08-12  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[build int] Decruft: Remove obsoleted configure.ac macro call.

	* configure.ac (GUILE_SITE_DIR): Delete macro call.

2011-08-12  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[expat] Fix bug: Declare smob sizes.

	* src/gi.h (DEFSMOB): Take arg ‘sz’.
	* src/expat.c (init_gexpat): Declare sizes
	of ‘XML-Parser’, ‘XML-Encoding’ smobs.

2011-08-12  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[inst] Also do sofix, uninstall-sofixed, mmc if !TRIPPY.

	* src/Makefile.am (fixargs): Bifurcate on ‘if TRIPPY’.
	(install-data-hook): Do sofix and mmc unconditionally.
	(uninstall-hook): Do uninstall-sofixed and mmc unconditionally.

2011-08-07  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[v] Use 5th edition of REC-xml.

	* test/REC-xml-20081126.xml: New file.
	* test/README: New file.
	* test/x001: Update coords for ‘xml->tree’ test.
	* test/REC-xml-19980210.xml: Delete file.

2011-08-05  Thien-Thi Nguyen  <ttn@gnuvola.org>

	Release: 0.5

	* configure.ac (AC_INIT): Bump version to "0.5" for release.

2011-08-05  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[maint] Delete obsoleted implementation.

	* Makefile.am (EXTRA_DIST): Remove xml.
	* generic_handlers.h: Delete file.
	* gexpat.c: Delete file.
	* gexpat.h: Delete file.
	* xml/expat.scm: Delete file.
	* xml/mixp.scm: Delete file.
	* xml/: Delete directory.

2011-08-05  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[doc] Use Guile-BAUX for doc maint; revamp content.

	* autogen.sh: Import Guile-BAUX modules ‘tsar’,
	‘c-tsar’, ‘tsin’; copy gnulib file doc/fdl.texi.
	* src/expat.c (parser_create, parser_create_ns): Tweak docs.
	* src/utils.scm: Likewise.  Also, throughout,
	rework ‘sig’ doc-comment option to use ‘args’.
	* src/Makefile.am (schemesource): New var.
	(EXTRA_DIST): Use $(schemesource).
	[MAINTAINER_MODE] (noinst_DATA): New var.
	[MAINTAINER_MODE] (all.snippets)
	(c.snippets, scheme.snippets): New targets.
	[MAINTAINER_MODE] (CLEANFILES): Add *.snippets.
	* doc/ref.texh: New file.
	* doc/mixp.texi: Change title; remove @smallbook; add self as
	another @author; remove @page; move @contents after title page;
	surround Top node with @ifnottex; don't use @ifinfo.
	(Top): Consolidate version info into blurb; rework
	menu description column; add "GNU FDL" to menu.
	(Introduction): Move FAQ url to footnote; use @uref;
	add another link to the expat project page; rewrite blurb.
	(Sample programs, Loading Mixp, Mixp components, How to...):
	Rework blurbs, examples; add @cindex entries.
	(Expat interface, High-level extensions): Delete these
	nodes and their children; replace with ‘@include ref.texi’.
	(GNU FDL): New node.
	(Index): Remove apology; delete @summarycontents, @contents.
	* doc/Makefile.am (mixp_TEXINFOS): New var.
	(EXTRA_DIST): Add ref.texh.
	[MAINTAINER_MODE] (bx, gx, SUFFIXES, snippets): New vars.
	[MAINTAINER_MODE] (.texh.texi): New old-style suffix rule.
	[MAINTAINER_MODE] (ref.texi): New target.

2011-08-05  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[expat] Use symbolic status, not numeric.

	* src/expat.c (statuses): New static ‘SCM’ array.
	(symbolic_status): New static func.
	(set_base, parse, parse_buffer): Use ‘symbolic_status’.
	(init_gexpat): Initialize ‘statuses’.
	* test/x001 (bad-parse?): Update.

2011-08-05  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[v int] Add abstraction: bad-parse?

	* test/x001 (bad-parse?): New alias for ‘zero?’; use it throughout.

2011-08-05  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[expat int] Store ‘#f’ for unspecified handlers.

	* src/expat.c (SPECIFIEDP): Delete macro.
	(mark_parser, make_parser, generic_element_start, generic_element_end)
	(generic_cdata_section_start, generic_cdata_section_end)
	(generic_namespace_decl_start, generic_namespace_decl_end):
	Use ‘NOT_FALSEP’.
	(hset_x): Use ‘SCM_BOOL_F’, ‘NOT_FALSEP’.

2011-08-05  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[expat int] Add abstraction: UNBOUND_MEANS_FALSE

	* src/expat.c (UNBOUND_MEANS_FALSE): New macro.
	(UNBOUND_MEANS_UNSPECIFIED, SPECIFIED_NOT_FALSE): Delete macros.
	(parse): Use ‘UNBOUND_MEANS_FALSE’, ‘NOT_FALSEP’.
	(parse_buffer, get_locus): Likewise.

2011-08-04  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[expat] Interpret ‘not-standalone’ rv #f to mean "signal error".

	* src/expat.c (generic_not_standalone): If the Scheme proc
	returns ‘#f’, return ‘XML_STATUS_ERROR’, else ‘XML_STATUS_OK’.

2011-08-04  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[expat] Fix bug: Handle NULL namespace decl prefix.

	* src/expat.c (generic_namespace_decl_start)
	(generic_namespace_decl_end): Use ‘STRMAYBE’ for ‘prefix’.

2011-08-04  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[v int] Rewrite ‘trace’ in x001.

	* test/x001 (trace): Rewrite to take
	format string + args; update all callers.

2011-08-04  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[expat int] Remove redundant ‘SPECIFIEDP’ checks.

	For non-pair (no -start, -end) handlers, setting the generic
	handler happens only if value satisfies init-time ‘PROCP’ check,
	so all the parse-time ‘SPECIFIEDP’ checks are redundant.

	* src/expat.c (generic_character_data)
	(generic_processing_instruction, generic_comment)
	(generic_default, generic_default_expand)
	(generic_unparsed_entity_decl, generic_notation_decl)
	(generic_not_standalone, generic_unknown_encoding): Don't
	bother checking that the handler is ‘SPECIFIEDP’; use it directly.

2011-08-04  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[expat int] Use XML_STATUS_* more.

	* src/expat.c (generic_not_standalone): Use ‘XML_STATUS_OK’.
	(generic_external_entity_ref): Likewise, and ‘XML_STATUS_ERROR’.
	(generic_unknown_encoding): Likewise.

2011-08-04  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[expat] Drop proc: set-encoding

	* src/expat.c (set_encoding): Delete func.
	* test/x001: Don't import ‘set-encoding’; don't test it.

2011-08-04  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[expat int] Use ‘INT_NUM’ and ‘C_INT’ more.

	* src/expat.c: Do ‘s/SCM_MAKINUM/INT_NUM/g’, ‘s/SCM_INUM/C_INT/g’.

2011-08-04  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[expat int] Do only one ‘scm_puts’ in print funcs.

	* src/expat.c (print_parser, print_encoding):
	Construct C string locally and ‘scm_puts’ once, at end.

2011-08-04  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[expat] Add proc: hget

	* src/expat.c (hget): New PRIMPROC.

2011-08-03  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[expat] Rename lastchoice{,-expand} to default{,-expand}.

	* src/expat.c (hnames): Use "default" and "default-expand"
	instead of "lastchoice" and "lastchoice-expand".
	(enum hindices): Rename all elements w/ prefix
	"hx_"; use ‘hx_default’ and ‘hx_default_expand’.
	(udsel): Prefix ‘h’ with "hx_".
	(generic_default): Rename from ‘generic_lastchoice’.
	(generic_default_expand): Rename from ‘generic_lastchoice_expand’.
	(hset_x): Update local macros to prefix "hx_"; use ‘default’
	and ‘default_expand’ instead of ‘lastchoice’ and ‘lastchoice_expand’.

2011-08-03  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[expat] Make ‘hset!’ take alist as required 2nd arg.

	* src/expat.c (hset_x): Rename 2nd arg to ‘alist’; change
	req-opt-var spec to 2-0-0; update validation / setting.
	* src/utils.scm (xml->tree): Update call to ‘expat:hset!’.
	* test/x001 (hset!/p): New proc; do ‘s|E:hset!|hset!/p|g’.

2011-08-03  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[utils int] Rewrite ‘xml->tree’.

	* src/utils.scm: Import ‘append-map’, ‘car+cdr’
	from (srfi srfi-1); import ‘(srfi srfi-11) let-values’.
	(xml->tree): Rewrite.
	(list->tree/one, list->tree, xml->list): Delete procs.

2011-08-03  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[utils] Internalize/prune list-related procs.

	These impose a profligate implementation strategy
	that we don't want to support long-term.

	* test/x001: Don't import ‘xml->list’, ‘list->tree/one’,
	‘tree->list/one’; remove tests that touch those procs.
	* src/utils.scm (tree->list/one, tree->list): Delete procs.
	(xml->list, list->tree/one, list->tree): Don't export.

2011-07-31  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[expat] Maintain attributes order.

	* src/expat.c (generic_element_start): Reverse
	the ‘acons’-built attributes alist if non-empty.

2011-07-30  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[v int] Use succint prefixes in ‘define-module’ form.

	* test/x001: Do ‘s/expat:/E:/g’, ‘s/mixp:/U:/g’.

2011-07-30  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[v int] Don't say "error code" in tests.

	* test/x001: Rework to avoid "error code" and ‘error-code’.

2011-07-30  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[expat] Rename ‘get-error-code’ to ‘error-symbol’.

	* src/expat.c (error_int_to_symbol): Delete func.
	(error_symbol): Rename from ‘get_error_code’; rename Scheme
	proc to ‘error-symbol’; incorporate ‘error_int_to_symbol’.
	* src/utils.scm: Do ‘s/get-error-code/error-symbol/g’.
	* test/x001: Likewise.

2011-07-30  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[expat] Drop proc: symbol<-error-code

	* src/expat.c (symbol_from_error_code): Delete func.
	* test/x001: Don't import ‘(mixp expat) symbol<-error-code’.
	("Test the error string reporting"): Drop ‘symbol<-error-code’ check.

2011-07-30  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[expat] Add some libexpat 2.0 error codes.

	* src/expat.c (error_codes) [2 == XML_MAJOR_VERSION]: Add
	‘XML_ERROR_RESERVED_PREFIX_XML’, ‘XML_ERROR_RESERVED_PREFIX_XMLNS’,
	‘XML_ERROR_RESERVED_NAMESPACE_URI’.

2011-07-30  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[expat] Fix bug: Make ‘error-string’ handle unrecognized codes.

	Strangely, ‘XML_ErrorString (XML_ERROR_NONE)’ => ‘NULL’.
	That is the unique value in ‘enum XML_Error’ manifesting this.

	* src/expat.c (error_string): Return ‘#f’ if ‘code’ is unrecognized.

2011-07-30  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[doc] Combine indices.

	* doc/mixp.texi: Combine function index into concept index.
	(Top): Update menu.
	(Index): New @node, from merging ‘Concept index’ and ‘Function index’.

2011-07-30  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[expat int] Add abstraction: UD_SIZE

	* src/expat.c (UD_SIZE): New #define.
	(free_parser, make_parser): Use ‘UD_SIZE’.

2011-07-29  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[doc int] Re-encode .texi in UTF-8.

	* mixp.texi: Specify document encoding as UTF-8;
	on save, re-encode in UTF-8 (was ISO-8859-1 aka LATIN-1).

2011-07-29  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[doc int] Remove "manual threading"; add missing ‘@top’ directive.

	* mixp.texi: For each ‘@node’ line, remove
	everything from the first comma to end of line.
	(Top): Add ‘@top’ directive.

2011-07-29  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[doc] Fix some improper ‘@xref’ usage.

	These were flagged by makeinfo (GNU texinfo) 4.13.

	* doc/mixp.texi (How to...): Use ‘@pxref’.
	(Expat handlers): Reword to put ‘@xref’ at end of sentence.
	(Error handling): Use ‘@pxref’.

2011-07-29  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[v int] Prune obsoleted infrastructure.

	* configure.ac (AC_CONFIG_FILES): Remove check-mixp.
	* check-mixp.in: Delete file.
	* guile-expat.c: Delete file.

2011-07-29  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[v] Import test/infrastructure from ttn-do 381 for "make check".

	* test/Makefile.am: New file.
	* test/runtest.in: New file.
	* test/x001: New file.
	* Makefile.am (SUBDIRS): Add test.
	(EXTRA_DIST): Remove test.
	(TESTS): Delete var.
	* configure.ac (AC_CONFIG_FILES): Add test/Makefile, test/runtest.
	* src/Makefile.am (stamp-all): New target.
	(CLEANFILES): Add stamp-all.

2011-07-29  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[build] Build from source in src/, not in top-level dir.

	* src/Makefile.am: New file.
	* build-aux/snuggle.m4: New file.
	* build-aux/sofix: New file.
	* build-aux/uninstall-sofixed: New file.
	* autogen.sh: Don't check/ensure build-aux/ existence;
	import Guile-BAUX modules for configure/build.
	* configure.ac (AC_CONFIG_HEADERS): Move config.h to src/.
	(DETERMINE_GUILE_LIBSITE_DIR): New macro call.
	(guile_cv_mkmodcat): New GUILE_TOOLS_EXISTSP for make-module-catalog.
	(mmc): New AC_SUBST.
	(TRIPPY): New AM_CONDITIONAL.
	(AC_CONFIG_FILES): Add src/Makefile.
	* Makefile.am (SUBDIRS): Add src.
	(lib_LTLIBRARIES, libmixp_la_SOURCES, libmixp_la_CFLAGS)
	(libmixp_la_LDFLAGS, libmixp_la_LIBADD, bin_PROGRAMS)
	(guile_expat_SOURCES, guile_expat_CFLAGS, guile_expat_LDFLAGS)
	(guile_expat_LDADD, noinst_HEADERS, BUILT_SOURCES)
	(subpkgdatadir, subpkgdata_DATA): Delete vars.
	(gexpat.x): Delete prereq decl.
	(SUFFIXES, SNARF): Delete vars.
	(.c.x): Delete old-style suffix rule.
	(MKDEP, CLEANFILES): Delete vars.
	(EXTRA_DIST): Add build-aux/sofix,
	build-aux/uninstall-sofixed, build-aux/guile-baux.

2011-07-29  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[int] Rename modules in new source; incorporate ttn-do dependecies.

	* src/utils.scm: Rename from mixp.scm; rename to (mixp utils).
	(fs, accumulator): New procs, from (ttn-do zzz personally).
	(ubs?): New object property, from (ttn-do zzz emacsdream).
	(recognizable-as-ubs): New proc, from same.
	(make-unsigned-byte-stream): Likewise; don't use ‘define*’;
	take optional arg ‘check’ instead of keyword arg.
	(utf8-reader, valid-ucs?, valid-ucs2?): Likewise.
	* src/expat.c (init_mixp_expat_module):
	Rename from ‘init_ttn_do_zz_xml_expat_module’.
	(MOD_INIT_LINK_THUNK): Update ref; rename to (mixp expat).

2011-07-28  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[int] Import variant source from ttn-do 381.

	See <http://www.gnuvola.org/software/ttn-do/> for details.

	* src/expat.c: New file.
	* src/gi.h: New file.
	* src/mixp.scm: New file.

2011-07-28  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[doc int] Add infrastructure for building documentation.

	* doc/Makefile.am: New file.
	* Makefile.am (SUBDIRS): New var.
	(EXTRA_DIST): Remove doc.
	* configure.ac (AC_CONFIG_FILES): Add doc/Makefile.

2011-07-28  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[v] Consoliate test files in test/.

	* samples/REC-xml-19980210.xml: Move from here...
	* test/REC-xml-19980210.xml: ...to here.
	* test/test-mixp.scm (test-mixp->): Update input file location.
	* Makefile.am (EXTRA_DIST): Remove samples.
	* samples/: Delete directory.

2011-07-27  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[boot] Modernize configure.ac et al a bit.

	* autogen.sh: Rewrite.
	* configure.ac (AC_PREREQ): Specify ‘2.67’.
	(AC_INIT): Use 3-arg form.
	(AC_CONFIG_AUX_DIR): New macro call.
	(AC_CONFIG_HEADERS): Rename from ‘AM_CONFIG_HEADER’.
	(AC_CONFIG_MACRO_DIR): New macro call.
	(AM_INIT_AUTOMAKE): Use 1-arg form.
	(AM_MAINTAINER_MODE): New macro call.
	(AC_ISC_POSIX, AC_PROG_LIBTOOL): Delete macro calls.
	(strerror): New AC_SEARCH_LIBS.
	(LT_PREREQ, LT_INIT): New macro calls.
	(AM_WITH_EXPAT): Replace macro call with expansion;
	use ‘AS_HELP_STRING’, ‘AS_IF’, etc.
	(AC_CONFIG_FILES): Add Makefile.
	(AC_OUTPUT): Use no-arg form.
	* Makefile.am (ACLOCAL_AMFLAGS): Update.
	(INCLUDES): Delete var.
	(EXTRA_DIST): Remove aclocal, INSTALL.old, INSTALL.
	* INSTALL, INSTALL.old: Delete files.
	* aclocal/guile.m4: Delete file.
	* aclocal/expat.m4: Delete file.
	* aclocal/: Delete directory.

2004-06-28  Dale P. Smith  <dsmith@sourceforge.net>

	[v] Adapt code to Guile 1.6 syntax.

	* test/test-mixp.scm: Changed all non-syntax unquoted () to '().
	Changed all calls to format to include the port (or #t/#f) as a
	first argument.

2003-01-15  Dmitry Morozhnikov  <dmiceman@mail.ru>

	Work around Guile 1.6.1 bug: Export (xml mixp) macros, too.

	Reported by Christian Neukirchen.

2003-01-09  Dmitry Morozhnikov  <dmiceman@mail.ru>

	Adapt to work with Guile 1.6.

	Reported by Christian Neukirchen.

2002-09-12  Dmitry Morozhnikov  <dmiceman@mail.ru>

	Also distribute INSTALL.

2002-09-12  Dmitry Morozhnikov  <dmiceman@mail.ru>

	Mention ‘--with-expat’ option to configure script in README.

2001-08-12  Seth Alves, Eric Hanchrow

	* Seth and Eric reported a compilation problem: CFLAGS included
	the standard directories before the expat directory, which could
	lead to compilation errors if another version of expat is
	installed somewhere else.

	* Other patches by Seth Alves seem to be necessary with the CVS
	version of Guile. See the INSTALL file.

2001-07-14  Thierry Bezecourt  <thbz@thbz.org>

        * No functionality modification. I only change my email address.

2000-08-30  Thierry Bezecourt  <thbz@thbz.cx>

	* mixp.texi: more documentation about handlers.

	* gexpat.c, generic_handlers.h: debugging of the external entity
	parsing.

2000-08-26  Thierry Bezecourt  <thbz@thbz.cx>

	* gexpat.c, generic_handlers.h: support for external entity
	parsing (see doc.)

	* xml/mixp.scm: added mixp:call-with-input-string and
	mixp:call-with-input-file

2000-08-01  Thierry Bezecourt  <thbz@thbz.cx>

	* doc/mixp.texi: bug fix in the documentation (Thanks to Olivier
	Buechel.)

2000-06-16  Thierry Bezecourt  <thbz@thbz.cx>

	* compilation: replaced deprecated function calls (Thanks to
	Olivier Buechel.)

2000-02-07  Thierry Bezecourt  <thbz@worldnet.fr>

	* xml/mixp.scm (mixp:tree->list, mixp:list->tree): added these
	procs.

	* gexpat.c (xml_get_error_code): fixed the return code of this
	function (which was simply wrong).

2000-01-27  Thierry Bezecourt  <thbz@worldnet.fr>

	* xml/mixp.scm (xml): added two procedures: mixp:xml->list to build a
	list of XML nodes from a port, mixp:xml->tree to build a tree
	of XML nodes from a port (the implementation is tail-recursive to
	avoid stack overflows).

	* gexpat.c, mixp.texi: removed expat:parser-free from the Scheme
	interface (the garbage collector will take care of that).

1999-12-22  Thierry Bezecourt  <thbz@worldnet.fr>

	* mixp.scm: added mixp:utf8->latin1 to convert UTF-8 strings
	(returned by the parser) to Latin-1 strings. Will fail if the
	original string is not a valid UTF-8 string, or if it contains
	non-Latin-1 characters.

	* mixp.scm: added other conversion functions: mixp:utf8->ucs2,
	mixp:utf8->ucs4


Copyright (C) 2011-2014, 2019-2021 Thien-Thi Nguyen

Copying and distribution of this file, with or without modification,
are permitted provided the copyright notice and this notice are preserved.
