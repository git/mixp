/* expat.c --- implement (mixp expat)

   Copyright (C) 2007, 2009-2012, 2014, 2019, 2020 Thien-Thi Nguyen
   Portions Copyright (C) 1999, 2000 Thierry Bézecourt

   This is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   This software is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.  */

/* Author: Thierry Bézecourt  */

#include "config.h"

#define XML_ATTR_INFO 1                 /* for XML_GetAttributeInfo (maybe) */
#include <expat.h>
/* Regularity is nice.  */
#define XML_SetNiceDefaultExpandHandler \
  XML_SetDefaultHandlerExpand
#define XML_SetNiceUnknownEncodingHandler(p,h) \
  XML_SetUnknownEncodingHandler (p, h, ud)

#include "gi.h"

#define STRMAYBE(x)  DEFAULT_FALSE (x, STRING (x))

#define UNBOUND_MEANS_FALSE(x) \
  if (! GIVENP (x)) x = SCM_BOOL_F

#define EXPAT_VERSION                           \
  ((XML_MAJOR_VERSION << 16)                    \
   | (XML_MINOR_VERSION << 8)                   \
   | (XML_MICRO_VERSION))


struct enumsym
{
  char *nm;
  SCM   sy;
};

#define PAIR(x)  { #x, SCM_UNSPECIFIED }

static SCM statuses[3];

static SCM
symbolic_status (enum XML_Status n)
{
  return statuses[n];
}

static const char const *
hnames[] =
{
  "element-start",
  "element-end",
  "character-data",
  "processing-instruction",
  "comment",
  "cdata-section-start",
  "cdata-section-end",
  "default",
  "default-expand",
  "doctype-decl-start",
  "doctype-decl-end",
  "entity-decl",
  "unparsed-entity-decl",
  "notation-decl",
  "namespace-decl-start",
  "namespace-decl-end",
  "not-standalone",
  "external-entity-ref",
  "skipped-entity",
  "unknown-encoding"
};

static size_t
n_hnames = sizeof (hnames) / sizeof (char *);

static SCM halist;

enum hindices
  {
    hx_element_start,
    hx_element_end,
    hx_character_data,
    hx_processing_instruction,
    hx_comment,
    hx_cdata_section_start,
    hx_cdata_section_end,
    hx_default,
    hx_default_expand,
    hx_doctype_decl_start,
    hx_doctype_decl_end,
    hx_entity_decl,
    hx_unparsed_entity_decl,
    hx_notation_decl,
    hx_namespace_decl_start,
    hx_namespace_decl_end,
    hx_not_standalone,
    hx_external_entity_ref,
    hx_skipped_entity,
    hx_unknown_encoding,
    hindex_count
  };

#define get_ud(x)  ((SCM *) XML_GetUserData (x))
#define as_ud(x)   ((SCM *) (x))

#define udsel(x,h)  (as_ud (x) [hx_ ## h])

/* Smob for XML_Parser.  */

static long parser_tag;

#define PARSERP(smob) \
  SCM_SMOB_PREDICATE (parser_tag, smob)
#define UNPACK_PARSER(smob) \
  ((XML_Parser) SMOBDATA (smob))

#define VALIDATE_PARSER()                                       \
  do {                                                          \
    ASSERT (parser, PARSERP (parser), SCM_ARG1);                \
    p = UNPACK_PARSER (parser);                                 \
  } while (0)

#define VALIDATE_PARSER_HANDLER()                               \
  do {                                                          \
    VALIDATE_PARSER ();                                         \
    ASSERT (handler, PROCP (handler), SCM_ARG2);                \
  } while (0)

#define VALIDATE_PARSER_START_END()                             \
  do {                                                          \
    VALIDATE_PARSER ();                                         \
    ASSERT (start, ! NOT_FALSEP (start) || PROCP (start),       \
            SCM_ARG2);                                          \
    ASSERT (end, ! NOT_FALSEP (end) || PROCP (end),             \
            SCM_ARG3);                                          \
  } while (0)

static SCM
mark_parser (SCM obj)
{
  XML_Parser p = UNPACK_PARSER (obj);
  int i; SCM *h;

  for (i = 0, h = get_ud (p); i < hindex_count; i++, h++)
    if (NOT_FALSEP (*h))
      scm_gc_mark (*h);
  RETURN_FALSE ();
}

#define UD_SIZE  (hindex_count * sizeof (SCM))

static size_t
free_parser (SCM obj)
{
  /* For the benefit of ‘GCFREE’.  */
  typedef struct { SCM h[hindex_count]; } ud_struct_t;

  XML_Parser p = UNPACK_PARSER (obj);

  GCFREE ((ud_struct_t *) get_ud (p), "parser-user-data");
  XML_SetUserData (p, NULL);
  XML_ParserFree (p);
  SCM_SET_SMOB_DATA (obj, (p = NULL));

  return GCRV (UD_SIZE);
}

static int
print_parser (SCM obj, SCM port, scm_print_state *pstate)
{
  char buf[32];

  snprintf (buf, 32, "#<XML-Parser %p>", (void *) UNPACK_PARSER (obj));
  scm_puts (buf, port);
  return 1;
}

static SCM
make_parser (XML_Parser p)
{
  int i; SCM *ud, *h;

  NOINTS ();
  ud = as_ud (GCMALLOC (UD_SIZE, "parser-user-data"));
  for (i = 0, h = ud; i < hindex_count; i++, h++)
    *h = SCM_BOOL_F;
  XML_SetUserData (p, ud);
  INTSOK ();

  SCM_RETURN_NEWSMOB (parser_tag, p);
}

/* Smob for XML_Encoding.  */

static long encoding_tag;

#define ENCODINGP(smob) \
  SCM_SMOB_PREDICATE (encoding_tag, smob)
#define UNPACK_ENCODING(smob) \
  ((XML_Encoding *) SMOBDATA (smob))

/* A structure used to wrap the data element of a XML_Encoding structure.  */
typedef struct
{
  /* A reference on the map in the XML_Encoding object, here only because
     generic_encoding_convert needs it.  Do not free it, it's really an
     alias for the map in the XML_Encoding object.  */
  int *map;

  /* A conversion function for multi-byte sequences.  */
  SCM convert;

  /* A release function called when the encoding is not used any more.  */
  SCM release;

} encoding_data;

#define get_ed(x)  ((encoding_data *) x->data)

static SCM
mark_encoding (SCM obj)
{
  encoding_data *xed = get_ed (UNPACK_ENCODING (obj));

  scm_gc_mark (xed->convert);

  return xed->release;
}

static size_t
free_encoding (SCM obj)
{
  XML_Encoding *enc = UNPACK_ENCODING (obj);

  GCFREE (get_ed (enc), "encoding-data");
  GCFREE (enc, "XML-Encoding");

  return GCRV (sizeof (XML_Encoding) + sizeof (encoding_data));
}

static int
print_encoding (SCM obj, SCM port, scm_print_state *pstate)
{
  char buf[4096];
  XML_Encoding *enc = UNPACK_ENCODING (obj);
  int w, i;

  w = snprintf (buf, 4096, "#<XML-Encoding");
  for (i = 0; i < 256; i++)
    w += snprintf (buf + w, 4096 - w, " %d", enc->map[i]);
  buf[w++] = '>';
  buf[w++] = '\0';
  scm_puts (buf, port);
  return 1;
}

static int
generic_encoding_convert (void *data, const char *s)
{
  encoding_data *xed = (encoding_data *) data;

  return C_INT
    (CALL1 (xed->convert, BSTRING (s, xed->map[(unsigned char) *s])));
}

static void
generic_encoding_release (void *data)
{
  encoding_data *xed = (encoding_data *) data;

  CALL0 (xed->release);
}


/* "Generic" handlers: whatever Scheme function was specified by the
   user, Expat sees the same handler function, and the Scheme handler
   is hidden in the user data.  */

static void
generic_element_start (void *data, const XML_Char *name,
                       const XML_Char ** atts)
{
  SCM handler = udsel (data, element_start);

  /* Note: the start handler is unbound if #f was specified as the
     second argument of set-element-handler.  */
  if (NOT_FALSEP (handler))
    {
      SCM alist = SCM_EOL;

      for (; atts[0] && atts[1]; atts += 2)
        alist = scm_acons (STRING (atts[0]),
                           STRING (atts[1]),
                           alist);
      if (! NULLP (alist))
        alist = scm_reverse_x (alist, SCM_EOL);

      CALL2 (handler, STRING (name), alist);
    }
}

static void
generic_element_end (void *data, const XML_Char *name)
{
  SCM handler = udsel (data, element_end);

  if (NOT_FALSEP (handler))
    CALL1 (handler, STRING (name));
}

static void
generic_character_data (void *data, const XML_Char *name, int len)
{
  CALL1 (udsel (data, character_data),
         BSTRING (name, len));
}

static void
generic_processing_instruction (void *data,
                                const XML_Char *target,
                                const XML_Char *pi_data)
{
  CALL2 (udsel (data, processing_instruction),
         STRING (target), STRING (pi_data));
}

static void
generic_comment (void *data, const XML_Char *comment_data)
{
  CALL1 (udsel (data, comment),
         STRING (comment_data));
}

static void
generic_cdata_section_start (void *data)
{
  SCM handler = udsel (data, cdata_section_start);

  if (NOT_FALSEP (handler))
    CALL0 (handler);
}

static void
generic_cdata_section_end (void *data)
{
  SCM handler = udsel (data, cdata_section_end);

  if (NOT_FALSEP (handler))
    CALL0 (handler);
}

static void
generic_default (void *data, const XML_Char *s, int len)
{
  CALL1 (udsel (data, default),
         BSTRING (s, len));
}

static void
generic_default_expand (void *data, const XML_Char *s, int len)
{
  CALL1 (udsel (data, default_expand),
         BSTRING (s, len));
}

static void
generic_doctype_decl_start (void *data,
                            const XML_Char *doctypeName,
                            const XML_Char *sysid,
                            const XML_Char *pubid,
                            int has_internal_subset)
{
  SCM arg1 = STRING (doctypeName);
  SCM arg2 = STRMAYBE (sysid);
  SCM arg3 = STRMAYBE (pubid);
  SCM arg4 = BOOLEAN (has_internal_subset);

  APPLY (udsel (data, doctype_decl_start),
         LISTIFY (arg1, arg2, arg3, arg4,
                  SCM_UNDEFINED));
}

static void
generic_doctype_decl_end (void *data)
{
  CALL0 (udsel (data, doctype_decl_end));
}

static void
generic_entity_decl (void *data,
                     const XML_Char *entityName,
                     int is_parameter_entity,
                     const XML_Char *value,
                     int value_length,
                     const XML_Char *base,
                     const XML_Char *systemId,
                     const XML_Char *publicId,
                     const XML_Char *notationName)
{
  SCM arg1 = STRING (entityName);
  SCM arg2 = BOOLEAN (is_parameter_entity);
  SCM arg3 = value ? BSTRING (value, value_length) : SCM_BOOL_F;
  SCM arg4 = STRMAYBE (base);
  SCM arg5 = STRMAYBE (systemId);
  SCM arg6 = STRMAYBE (publicId);
  SCM arg7 = STRMAYBE (notationName);

  APPLY (udsel (data, entity_decl),
         LISTIFY (arg1, arg2, arg3, arg4, arg5, arg6, arg7,
                  SCM_UNDEFINED));
}

static void
generic_unparsed_entity_decl (void *data,
                              const XML_Char *entityName,
                              const XML_Char *base,
                              const XML_Char *systemId,
                              const XML_Char *publicId,
                              const XML_Char *notationName)
{
  SCM arg1 = STRMAYBE (entityName);
  SCM arg2 = STRMAYBE (base);
  SCM arg3 = STRMAYBE (systemId);
  SCM arg4 = STRMAYBE (publicId);
  SCM arg5 = STRMAYBE (notationName);

  APPLY (udsel (data, unparsed_entity_decl),
         LISTIFY (arg1, arg2, arg3, arg4, arg5,
                  SCM_UNDEFINED));
}

static void
generic_notation_decl (void *data,
                       const XML_Char *notationName,
                       const XML_Char *base,
                       const XML_Char *systemId,
                       const XML_Char *publicId)
{
  SCM arg1 = STRING (notationName);
  SCM arg2 = STRMAYBE (base);
  SCM arg3 = STRMAYBE (systemId);
  SCM arg4 = STRMAYBE (publicId);

  APPLY (udsel (data, notation_decl),
         LISTIFY (arg1, arg2, arg3, arg4,
                  SCM_UNDEFINED));
}

static void
generic_namespace_decl_start (void *data,
                              const XML_Char *prefix,
                              const XML_Char *uri)
{
  SCM handler = udsel (data, namespace_decl_start);

  if (NOT_FALSEP (handler))
    CALL2 (handler, STRMAYBE (prefix), STRING (uri));
}

static void
generic_namespace_decl_end (void *data, const XML_Char *prefix)
{
  SCM handler = udsel (data, namespace_decl_end);

  if (NOT_FALSEP (handler))
    CALL1 (handler, STRMAYBE (prefix));
}

static int
generic_not_standalone (void *data)
{
  return SCM_FALSEP (CALL0 (udsel (data, not_standalone)))
    ? XML_STATUS_ERROR
    : XML_STATUS_OK;
}

static int
generic_external_entity_ref (XML_Parser p,
                             const XML_Char *context,
                             const XML_Char *base,
                             const XML_Char *systemId,
                             const XML_Char *publicId)
{
  XML_Parser ext_p;
  SCM port, nl, buf;
  range_t cbuf;
  enum XML_Status rv = XML_STATUS_OK;
  int donep = 0;

  port = APPLY (udsel (get_ud (p), external_entity_ref),
                LISTIFY (STRMAYBE (context),
                         STRMAYBE (base),
                         STRMAYBE (systemId),
                         STRMAYBE (publicId),
                         SCM_UNDEFINED));

  SCM_ASSERT (SCM_NIMP (port) && SCM_OPINPORTP (port),
              port, SCM_ARGn, "external-entity-ref handler");

  ext_p = XML_ExternalEntityParserCreate (p, context, NULL);

#define NEWBUF(size) \
  scm_make_string (NUM_INT (size), SCM_MAKE_CHAR (0))

  nl = STRING ("\n");
  buf = NEWBUF (1024);

  while (!donep)
    {
      enum XML_Status parse_res;
      size_t nbytes = 0;
      SCM delim, big = SCM_EOL;

#define SHORTP()  (! NOT_FALSEP (delim))
#define EXTRAP()  (! NULLP (big))

      do
        {
          SCM pair = scm_read_delimited_x (nl, buf, SCM_BOOL_T, port,
                                           SCM_UNDEFINED, SCM_UNDEFINED);
          delim   =        CAR (pair);
          nbytes += C_INT (CDR (pair));
          if (SHORTP () || EXTRAP ())
            {
              big = CONS (buf, big);
              buf = NEWBUF (256);
            }
        }
      while (SHORTP ());

      if (EXTRAP ())
        buf = scm_string_append (scm_reverse_x (big, SCM_EOL));

#undef SHORTP
#undef EXTRAP

      FINANGLE_RAW (buf);
      parse_res = XML_Parse (ext_p, RS (buf), nbytes,
                             (donep = (delim == SCM_EOF_VAL)));
      UNFINANGLE (buf);

      if (XML_STATUS_OK != parse_res)
        {
          rv = XML_STATUS_ERROR;
          donep = 1;
        }
    }

#undef NEWBUF

  XML_ParserFree (ext_p);
  return rv;
}

static void
generic_skipped_entity (void *data,
                        const XML_Char *entityName,
                        int is_parameter_entity)
{
  CALL2 (udsel (data, skipped_entity),
         STRING (entityName),
         BOOLEAN (is_parameter_entity));
}

static int
generic_unknown_encoding (void *data,
                          const XML_Char *name,
                          XML_Encoding *info)
{
  enum XML_Status res = XML_STATUS_ERROR;
  SCM encoding_info = CALL1 (udsel (data, unknown_encoding),
                             STRING (name));

  if (ENCODINGP (encoding_info))
    {
      XML_Encoding *tmp = UNPACK_ENCODING (encoding_info);

     *info = *tmp;
     /* Inform expat that the XML_Encoding structure is valid.  */
     res = XML_STATUS_OK;
    }

  return res;
}


/* Scheme interface.  */

PRIMPROC
(expat_version, "expat-version", 0, 0, 0,
 (void),
 doc: /***********
Return a list containing version info of this expat.
The list has the form:
@code{(@var{major} @var{minor} @var{micro} @var{string})}
where @var{major}, @var{minor}, @var{micro} are integers;
and @var{string} is a string that begins w/ "expat_" and
ends with the major, minor, micro numbers in dotted notation.

@example
\(expat-version) @result{} (2 2 6 "expat_2.2.6")
@end example  */)
{
#define FUNC_NAME s_expat_version
  XML_Expat_Version ver = XML_ExpatVersionInfo ();
  SCM str = STRING (XML_ExpatVersion ());

  return LIST4 (NUM_INT (ver.major),
                NUM_INT (ver.minor),
                NUM_INT (ver.micro),
                str);
#undef FUNC_NAME
}

PRIMPROC
(get_feature_list, "get-feature-list", 0, 0, 0,
 (void),
 doc: /***********
Return an alist describing the features of the underlying libexpat.
The alist keys are strings, one or more of:

@example
XML_UNICODE
XML_UNICODE_WCHAR_T
XML_DTD
XML_CONTEXT_BYTES
XML_MIN_SIZE
sizeof(XML_Char)
sizeof(XML_LChar)
XML_NS
XML_LARGE_SIZE
XML_ATTR_INFO
@end example

The values are absent if the feature is a simple one,
otherwise some feature-specific positive integer.

Additionally, the first pair has key @code{EXPAT_VERSION}
and value a number which (in hex) represents the version
numbers of the underlying libexpat.  For example:

@example
131585 @result{} #x020201 @result{} version 2.2.1
@end example  */)
{
#define FUNC_NAME s_get_feature_list
  const XML_Feature *f;
  SCM rv;

  for (f = XML_GetFeatureList (), rv = SCM_EOL;
       f && f->feature;
       f++)
    {
      long int val = f->value;

      rv = scm_acons (STRING (f->name),
                      (val > 0
                       ? NUM_INT (val)
                       : SCM_EOL),
                      rv);
    }

  rv = scm_acons (STRING ("EXPAT_VERSION"),
                  NUM_INT (EXPAT_VERSION),
                  rv);

  return rv;
#undef FUNC_NAME
}

PRIMPROC
(parser_p, "parser?", 1, 0, 0,
 (SCM obj),
 doc: /***********
Return @code{#t} if @var{obj} is an XML-Parser object,
otherwise @code{#f}.  */)
{
  return BOOLEAN (PARSERP (obj));
}

/* Guile 1.8.x ‘SCM_VALIDATE_VECTOR_LEN’ uses deprecated
   macros ‘SCM_VECTORP’ and ‘SCM_VECTOR_LENGTH’ internally.
   Work around this oversight.  */
#if GI_LEVEL_PRECISELY_1_8
#undef SCM_VALIDATE_VECTOR_LEN
#define SCM_VALIDATE_VECTOR_LEN(pos, v, len)            \
  SCM_ASSERT (scm_is_vector (v)                         \
              && len == scm_c_vector_length (v),        \
              v, pos, FUNC_NAME)
#endif  /* GI_LEVEL_PRECISELY_1_8 */

PRIMPROC
(make_encoding, "make-xml-encoding", 3, 0, 0,
 (SCM map, SCM convert, SCM release),
 doc: /***********
Return a new XML-Encoding object.

@var{map} is a vector of length 256.  Each element is an integer
specifying how many bytes are required to decode a multibyte
``character'' whose first byte is that element's index.

@var{convert} is a proc that takes one arg, a unibyte string.
It should return the "Unicode scalar value" of the string,
or -1 if the byte sequence is malformed.

@var{release} is a thunk the parser calls when done
all conversion work.  */)
{
#define FUNC_NAME s_make_encoding
  XML_Encoding *enc = NULL;
  const SCM *map_elts;
  encoding_data *xe_data;
  int i;

  SCM_VALIDATE_VECTOR_LEN (1, map, 256);
  SCM_VALIDATE_PROC (2, convert);
  SCM_VALIDATE_PROC (3, release);

  NOINTS ();
  enc = GCMALLOC (sizeof (XML_Encoding), "XML-Encoding");
  INTSOK ();

  /* The map goes directly into the XML_Encoding object.  */
  {
#if ! GI_LEVEL_1_8
    map_elts = SCM_VELTS (map);
#else
    scm_t_array_handle handle;

    scm_array_get_handle (map, &handle);
    map_elts = scm_array_handle_elements (&handle);
#endif
    for (i = 0; i < 256; i++)
      enc->map[i] = C_INT (map_elts[i]);
#if GI_LEVEL_1_8
    scm_array_handle_release (&handle);
#endif
  }

  NOINTS ();
  enc->data = GCMALLOC (sizeof (encoding_data), "encoding-data");
  INTSOK ();

  xe_data = get_ed (enc);
  xe_data->map = enc->map;
  xe_data->convert = convert;
  xe_data->release = release;

  enc->convert = generic_encoding_convert;
  enc->release = generic_encoding_release;

  SCM_RETURN_NEWSMOB (encoding_tag, enc);
#undef FUNC_NAME
}

PRIMPROC
(parser_create, "parser-create", 0, 1, 0,
 (SCM encoding),
 doc: /***********
Return a new parser object.
Optional arg @var{encoding} is a string specifying
the encoding to use (for example, "UTF-8").  */)
{
#define FUNC_NAME s_parser_create
  range_t cencoding;
  XML_Char *e = NULL;
  SCM rv;

  if (GIVENP (encoding))
    {
      SCM_VALIDATE_STRING (1, encoding);
      FINANGLE (encoding);
      e = RS (encoding);
    }
  rv = make_parser (XML_ParserCreate (e));
  if (e)
    UNFINANGLE (encoding);
  return rv;
#undef FUNC_NAME
}

PRIMPROC
(parser_create_ns, "parser-create-ns", 0, 2, 0,
 (SCM encoding, SCM namespace_separator),
 doc: /***********
Optional arg @var{encoding} is a string specifying the encoding to use.
Second optional arg @var{namespace-separator} is a character used to
separate the namespace part from the local part (e.g., @code{#\:}).

Note: Using this proc (instead of @code{parser-create}) enables dispatch
to the @code{namespace-decl-start} and @code{namespace-decl-end}
handlers.  */)
{
#define FUNC_NAME s_parser_create_ns
  range_t cencoding;
  XML_Char *e = NULL;
  XML_Char c = '\0';
  SCM rv;

  if (GIVENP (encoding))
    {
      SCM_VALIDATE_STRING (1, encoding);
      FINANGLE (encoding);
      e = RS (encoding);
    }
  if (GIVENP (namespace_separator))
    SCM_VALIDATE_CHAR_COPY (2, namespace_separator, c);

  rv = make_parser (XML_ParserCreateNS (e, c));
  if (e)
    UNFINANGLE (encoding);
  return rv;
#undef FUNC_NAME
}

PRIMPROC
(set_encoding, "set-encoding", 2, 0, 0,
 (SCM parser, SCM encoding),
 doc: /***********
Set the encoding for @var{parser} to @var{encoding}.
Return @code{XML_STATUS_OK} for success.
This is like calling @code{parser-create} (@pxref{Parser})
with @var{encoding} as the first arg.

@strong{NB}: Calling @code{set-encoding} after @code{parse} or
@code{parse-buffer} has no effect and returns @code{XML_STATUS_ERROR}.  */)
{
#define FUNC_NAME s_set_encoding
  XML_Parser p;
  range_t cencoding;
  enum XML_Status status;

  VALIDATE_PARSER ();
  SCM_VALIDATE_STRING (2, encoding);

  FINANGLE (encoding);
  status = XML_SetEncoding (p, RS (encoding));
  UNFINANGLE (encoding);
  return symbolic_status (status);
#undef FUNC_NAME
}

PRIMPROC
(default_current, "default-current", 1, 0, 0,
 (SCM parser),
 doc: /***********
Declare that @var{parser} is the default current parser.  */)
{
#define FUNC_NAME s_default_current
  XML_Parser p;

  VALIDATE_PARSER ();

  XML_DefaultCurrent (p);

  RETURN_UNSPECIFIED ();
#undef FUNC_NAME
}

PRIMPROC
(hset_x, "hset!", 1, 0, 1,
 (SCM parser, SCM plist),
 doc: /***********
Set handlers for @var{parser} as specified in @var{plist},
a list of alternating handler names (symbols) and values.
Valid values are a procedure, @code{()} (the empty list) or @code{#f}.
Note, however, that no arity checks are done on the procedures.

As a special backward-compatible case, if the first key is a full alist,
use that instead to specify the handlers to set, and ignore the rest of
the args.
@strong{NB}: Support for this calling convention @strong{WILL BE REMOVED}
by the 1.0 release of Mixp.  */)
{
#define FUNC_NAME s_hset_x
  XML_Parser p;
  SCM alist;
  SCM *ud;
  SCM ls, pair, sym, val;

  VALIDATE_PARSER ();
  SCM_VALIDATE_CONS (2, plist);

  if (SYMBOLP (CAR (plist)))
    {
      alist = SCM_BOOL_F;

      /* First pass: validate ‘plist’.  */
      for (ls = plist; ! NULLP (ls); ls = CDR (ls))
        {
          sym = CAR (ls);
          SCM_VALIDATE_SYMBOL (2, sym);
          ASSERT (sym, NOT_FALSEP (scm_assq (sym, halist)), 2);
          ls = CDR (ls);
          val = CAR (ls);
          ASSERT (val, NULLP (val) || SCM_FALSEP (val) || PROCP (val), 2);
        }
    }
  else
    {
      alist = CAR (plist);

      /* First pass: validate ‘alist’.  */
      for (ls = alist; ! NULLP (ls); ls = CDR (ls))
        {
          pair = CAR (ls);
          SCM_VALIDATE_CONS (2, pair);
          sym = CAR (pair);
          SCM_VALIDATE_SYMBOL (2, sym);
          ASSERT (sym, NOT_FALSEP (scm_assq (sym, halist)), 2);
          val = CDR (pair);
          ASSERT (val, NULLP (val) || SCM_FALSEP (val) || PROCP (val), 2);
        }
    }

  /* Second pass: do it!  */
  ud = get_ud (p);
  for (ls = SCM_NFALSEP (alist) ? alist : plist;
       ! NULLP (ls);
       ls = CDR (ls))
    {
      int usep;

      if (SCM_NFALSEP (alist))
        {
          pair = CAR (ls);
          sym = CAR (pair);
          val = CDR (pair);
        }
      else
        {
          sym = CAR (ls);
          ls = CDR (ls);
          val = CAR (ls);
        }

#define SETH(x)                                 \
      usep = PROCP (val);                       \
      udsel (ud, x) = usep ? val : SCM_BOOL_F

#define M(x)  (usep ? x : NULL)         /* (maybe) */

#define SETG1(camel,h)                                          \
      case hx_ ## h:                                            \
        {                                                       \
          SETH (h);                                             \
          XML_Set ## camel ## Handler (p, M (generic_ ## h));   \
        }                                                       \
      break

#define SETG2(camel,h,which)                                    \
      case hx_ ## h ## _ ## which:                              \
        {                                                       \
          SETH (h ## _ ## which);                               \
          usep = (NOT_FALSEP (udsel (ud, h ## _start)) ||       \
                  NOT_FALSEP (udsel (ud, h ## _end)));          \
          XML_Set ## camel ## Handler                           \
            (p,                                                 \
             M (generic_ ## h ## _start),                       \
             M (generic_ ## h ## _end));                        \
        }                                                       \
      break

      switch (C_INT (CDR (scm_assq (sym, halist))))
        {
          SETG2 (Element, element, start);
          SETG2 (Element, element, end);
          SETG1 (CharacterData, character_data);
          SETG1 (ProcessingInstruction, processing_instruction);
          SETG1 (Comment, comment);
          SETG2 (CdataSection, cdata_section, start);
          SETG2 (CdataSection, cdata_section, end);
          SETG1 (Default, default);
          SETG1 (NiceDefaultExpand, default_expand);
          SETG2 (DoctypeDecl, doctype_decl, start);
          SETG2 (DoctypeDecl, doctype_decl, end);
          SETG1 (EntityDecl, entity_decl);
          SETG1 (UnparsedEntityDecl, unparsed_entity_decl);
          SETG1 (NotationDecl, notation_decl);
          SETG2 (NamespaceDecl, namespace_decl, start);
          SETG2 (NamespaceDecl, namespace_decl, end);
          SETG1 (NotStandalone, not_standalone);
          SETG1 (ExternalEntityRef, external_entity_ref);
          SETG1 (SkippedEntity, skipped_entity);
          SETG1 (NiceUnknownEncoding, unknown_encoding);
        }

#undef SETH
#undef M
#undef SETG1
#undef SETG2
    }

  RETURN_UNSPECIFIED ();
#undef FUNC_NAME
}

PRIMPROC
(hget_one, "hget-one", 2, 0, 0,
 (SCM parser, SCM handler),
 doc: /***********
Return the procedure set as @var{parser}'s @var{handler} (a symbol).
If none is set, return @code{#f}.  */)
{
#define FUNC_NAME s_hget_one
  XML_Parser p;
  SCM val;

  VALIDATE_PARSER ();
  ASSERT (handler, NOT_FALSEP (val = scm_assq (handler, halist)), 2);

  val = as_ud (get_ud (p))[C_INT (CDR (val))];
  return PROCP (val)
    ? val
    : SCM_BOOL_F;
#undef FUNC_NAME
}

PRIMPROC
(hget, "hget", 1, 0, 0,
 (SCM parser),
 doc: /***********
Return an alist representing the handler set of @var{parser}.
If a particular handler is not specified, that pair's @sc{cdr}
will be @code{#f}.  The alist keys are handler names.  */)
{
#define FUNC_NAME s_hget
  XML_Parser p;
  int i = hindex_count;
  SCM *ud;
  SCM rv, sym, val;

  VALIDATE_PARSER ();
  ud = get_ud (p);
  rv = SCM_EOL;
  while (i--)
    {
      sym = SYMBOL (hnames[i]);
      val = as_ud (ud)[i];
      if (! PROCP (val))
        val = SCM_BOOL_F;
      rv = scm_acons (sym, val, rv);
    }
  return rv;
#undef FUNC_NAME
}

PRIMPROC
(set_hash_salt, "set-hash-salt", 2, 0, 0,
 (SCM parser, SCM salt),
 doc: /***********
Set the @var{parser}'s hash salt to @var{salt}, an unsigned integer.
This number is used for internal hash calculations.
Setting it helps prevent DoS attacks
based on predicting hash function behavior.
Return @code{#t} if successful (called before parsing has started),
@code{#f} otherwise.  */)
{
#define FUNC_NAME s_set_hash_salt
  XML_Parser p;
  unsigned long int csalt;              /* Aye! */

  VALIDATE_PARSER ();
  SCM_VALIDATE_ULONG_COPY (2, salt, csalt);

  return BOOLEAN (XML_SetHashSalt (p, csalt));
#undef FUNC_NAME
}

PRIMPROC
(set_base, "set-base", 2, 0, 0,
 (SCM parser, SCM base),
 doc: /***********
Set base for @var{parser} to @var{base} (a string).
Return a symbolic status.  */)
{
#define FUNC_NAME s_set_base
  XML_Parser p;
  range_t cbase;
  enum XML_Status status;

  VALIDATE_PARSER ();
  SCM_VALIDATE_STRING (1, base);

  FINANGLE (base);
  status = XML_SetBase (p, RS (base));
  UNFINANGLE (base);
  return symbolic_status (status);
#undef FUNC_NAME
}

PRIMPROC
(get_base, "get-base", 1, 0, 0,
 (SCM parser),
 doc: /***********
Return the base (a string) of @var{parser}.
If none is set, return @code{#f}.  */)
{
#define FUNC_NAME s_get_base
  const char *base;
  XML_Parser p;

  VALIDATE_PARSER ();

  base = XML_GetBase (p);
  return STRMAYBE (base);
#undef FUNC_NAME
}

PRIMPROC
(get_specified_attribute_count, "get-specified-attribute-count", 1, 0, 0,
 (SCM parser),
 doc: /***********
Get the specified attribute count for @var{parser}.  */)
{
#define FUNC_NAME s_get_specified_attribute_count
  XML_Parser p;

  VALIDATE_PARSER ();

  return NUM_INT (XML_GetSpecifiedAttributeCount (p));
#undef FUNC_NAME
}

static SCM sym_u64;

PRIMPROC
(get_attribute_info, "get-attribute-info", 1, 0, 0,
 (SCM parser),
 doc: /***********
Return a 2-D array describing the attributes of the current element.
Each element of the array is an unsigned integer representing the
byte offset of the attribute and value start and end positions.
The end positions are ``one past the last byte''.
The array has the form:

@example
attr1-attr-start  attr1-attr-end  attr1-val-start  attr1-val-end
attr2-attr-start  attr2-attr-end  attr2-val-start  attr2-val-end
      ...               ...             ...              ...
attrN-attr-start  attrN-attr-end  attrN-val-start  attrN-val-end
@end example

The attribute count (number @var{n}) is half the value that
@code{get-sepecified-attribute-count} returns.

@emph{API Note}:
If the underlying libexpat is prior to 2.1.0, or if it doesn't
provide @code{XML_GetAttributeInfo}, return simply @code{#f}.
You can determine this from the absence of @code{XML_ATTR_INFO} from
the return value of @code{get-feature-list} (@pxref{Expat interface}).  */)
{
#define FUNC_NAME s_get_attribute_info
#if 0x020100 <= EXPAT_VERSION && defined (HAVE_GETATTRIBUTEINFO)
  /* v 2.1.0 and later */
  XML_Parser p;
  unsigned int nrows, i;
  SCM bounds, two, rv;
  const XML_AttrInfo *oprv;

  VALIDATE_PARSER ();

  /* First allocate.  */
  nrows = XML_GetSpecifiedAttributeCount (p) / 2;
  bounds = LIST2 (NUM_INT (nrows), NUM_INT (4));
  two = CDR (bounds);
  rv = scm_make_typed_array (sym_u64, NUM_INT (0), bounds);

  /* Next, do the op and fill the array.  */
  oprv = XML_GetAttributeInfo (p);
  for (i = 0; i < nrows; i++)
    {
#define JAM(idx,member)  do                                             \
        {                                                               \
          SETCAR (two, NUM_INT (idx));                                  \
          scm_array_set_x (rv, NUM_ULONG (oprv[i].member), bounds);     \
        } while (0)

      SETCAR (bounds, NUM_INT (i));
      JAM (0, nameStart);
      JAM (1, nameEnd);
      JAM (2, valueStart);
      JAM (3, valueEnd);

#undef JAM
    }

  /* Lastly, return the array.  */
  return rv;

#else  /* prior to v 2.1.0, or XML_GetAttributeInfo unavailable */

  RETURN_FALSE ();

#endif /* prior to v 2.1.0, or XML_GetAttributeInfo unavailable */
#undef FUNC_NAME
}

PRIMPROC
(parse, "parse", 2, 1, 0,
 (SCM parser, SCM s, SCM finalp),
 doc: /***********
Use @var{parser} to parse string @var{s}.
Optional third arg @var{finalp}, if non-@code{#f}, means
this call is the last parsing to be done on @var{s}.
Return a symbolic status.  */)
{
#define FUNC_NAME s_parse
  XML_Parser p;
  range_t cs;
  enum XML_Status status;

  VALIDATE_PARSER ();

  SCM_VALIDATE_STRING (2, s);
  UNBOUND_MEANS_FALSE (finalp);

  FINANGLE_RAW (s);
  status = XML_Parse (p, RS (s), RLEN (s), NOT_FALSEP (finalp));
  UNFINANGLE (s);
  return symbolic_status (status);
#undef FUNC_NAME
}

PRIMPROC
(parse_buffer, "parse-buffer", 2, 1, 0,
 (SCM parser, SCM len, SCM finalp),
 doc: /***********
Use @var{parser} to parse @var{len} bytes of the internal buffer.
Optional third arg @var{finalp}, if non-@code{#f},
means this call is the last parsing to be done.
Return a symbolic status.  */)
{
#define FUNC_NAME s_parse_buffer
  XML_Parser p;
  int clen;                            /* ∄ ‘SCM_VALIDATE_INT’.  */

  VALIDATE_PARSER ();
  SCM_VALIDATE_INT_COPY (2, len, clen);
  UNBOUND_MEANS_FALSE (finalp);

  return symbolic_status
    (XML_ParseBuffer (p, clen, NOT_FALSEP (finalp)));
#undef FUNC_NAME
}

PRIMPROC
(stop_parser, "stop-parser", 2, 0, 0,
 (SCM parser, SCM resumable),
 doc: /***********
Stop @var{parser}.  Return a symbolic status.  If @var{resumable}
is non-@code{#f}, parsing is suspended.  If @code{#f}, parsing is
aborted.

This should be called from a handler (e.g., @code{element-start}).
Note that some handlers will continue to be called before fully
stopping (e.g., @code{element-end}).  */)
{
#define FUNC_NAME s_stop_parser
  XML_Parser p;

  VALIDATE_PARSER ();

  return symbolic_status (XML_StopParser (p, C_BOOL (resumable)));
#undef FUNC_NAME
}

PRIMPROC
(resume_parser, "resume-parser", 1, 0, 0,
 (SCM parser),
 doc: /***********
Resume @var{parser}.  Return a symbolic status, the same as
for @code{parse} or @code{parse-buffer}, with the addition of
@code{XML_ERROR_NOT_SUSPENDED}.

This should not be called from a handler.  It should be called
first on the most deeply nested child parser, then successively
on the parent parser(s).  */)
{
#define FUNC_NAME s_resume_parser
  XML_Parser p;

  VALIDATE_PARSER ();

  return symbolic_status (XML_ResumeParser (p));
#undef FUNC_NAME
}

static SCM parsing_statuses[4];

PRIMPROC
(get_parsing_status, "get-parsing-status", 1, 0, 0,
 (SCM parser),
 doc: /***********
Return list @code{(@var{status} @var{final-buffer?})}, where @var{status}
is the symbolic status of @var{parser} with respect to being initialized,
parsing, finished, or suspended; and @var{final-buffer} is non-@code{#f} if
processing is occurring for the final buffer.  */)
{
#define FUNC_NAME s_get_parsing_status
  XML_Parser p;
  XML_ParsingStatus status;

  VALIDATE_PARSER ();

  XML_GetParsingStatus (p, &status);
  return LIST2 (parsing_statuses[status.parsing],
                BOOLEAN (status.finalBuffer));
#undef FUNC_NAME
}

static struct enumsym
param_entity_parsing_codes[] =
{
  PAIR (XML_PARAM_ENTITY_PARSING_NEVER),
  PAIR (XML_PARAM_ENTITY_PARSING_UNLESS_STANDALONE),
  PAIR (XML_PARAM_ENTITY_PARSING_ALWAYS)
};

static int
  param_entity_parsing_codes_count =
  sizeof (param_entity_parsing_codes) / sizeof (struct enumsym);

PRIMPROC
(set_param_entity_parsing, "set-param-entity-parsing", 2, 0, 0,
 (SCM parser, SCM code),
 doc: /***********
Set entity parsing for @var{parser} to @var{code} (a symbol).
Valid values for @var{code} are:

@example
XML_PARAM_ENTITY_PARSING_NEVER
XML_PARAM_ENTITY_PARSING_UNLESS_STANDALONE
XML_PARAM_ENTITY_PARSING_ALWAYS
@end example

This controls parsing of parameter entities (including the external
DTD subset).  See @file{/usr/include/expat.h} for more information.  */)
{
#define FUNC_NAME s_set_param_entity_parsing
  XML_Parser p;
  int intcode = -1;
  int i;

  VALIDATE_PARSER ();

  for (i = 0; i < param_entity_parsing_codes_count; i++)
    {
      if (EQ (param_entity_parsing_codes[i].sy, code))
        {
          intcode = i;
          break;
        }
    }

  return NUM_INT (XML_SetParamEntityParsing (p, intcode));
#undef FUNC_NAME
}

static struct enumsym
error_codes[] =
{
  PAIR (XML_ERROR_NONE),
  PAIR (XML_ERROR_NO_MEMORY),
  PAIR (XML_ERROR_SYNTAX),
  PAIR (XML_ERROR_NO_ELEMENTS),
  PAIR (XML_ERROR_INVALID_TOKEN),
  PAIR (XML_ERROR_UNCLOSED_TOKEN),
  PAIR (XML_ERROR_PARTIAL_CHAR),
  PAIR (XML_ERROR_TAG_MISMATCH),
  PAIR (XML_ERROR_DUPLICATE_ATTRIBUTE),
  PAIR (XML_ERROR_JUNK_AFTER_DOC_ELEMENT),
  PAIR (XML_ERROR_PARAM_ENTITY_REF),
  PAIR (XML_ERROR_UNDEFINED_ENTITY),
  PAIR (XML_ERROR_RECURSIVE_ENTITY_REF),
  PAIR (XML_ERROR_ASYNC_ENTITY),
  PAIR (XML_ERROR_BAD_CHAR_REF),
  PAIR (XML_ERROR_BINARY_ENTITY_REF),
  PAIR (XML_ERROR_ATTRIBUTE_EXTERNAL_ENTITY_REF),
  PAIR (XML_ERROR_MISPLACED_XML_PI),
  PAIR (XML_ERROR_UNKNOWN_ENCODING),
  PAIR (XML_ERROR_INCORRECT_ENCODING),
  PAIR (XML_ERROR_UNCLOSED_CDATA_SECTION),
  PAIR (XML_ERROR_EXTERNAL_ENTITY_HANDLING),
  PAIR (XML_ERROR_NOT_STANDALONE),
  PAIR (XML_ERROR_UNEXPECTED_STATE),
  PAIR (XML_ERROR_ENTITY_DECLARED_IN_PE),
  PAIR (XML_ERROR_FEATURE_REQUIRES_XML_DTD),
  PAIR (XML_ERROR_CANT_CHANGE_FEATURE_ONCE_PARSING),
  PAIR (XML_ERROR_UNBOUND_PREFIX),
  PAIR (XML_ERROR_UNDECLARING_PREFIX),
  PAIR (XML_ERROR_INCOMPLETE_PE),
  PAIR (XML_ERROR_XML_DECL),
  PAIR (XML_ERROR_TEXT_DECL),
  PAIR (XML_ERROR_PUBLICID),
  PAIR (XML_ERROR_SUSPENDED),
  PAIR (XML_ERROR_NOT_SUSPENDED),
  PAIR (XML_ERROR_ABORTED),
  PAIR (XML_ERROR_FINISHED),
  PAIR (XML_ERROR_SUSPEND_PE)
#if 0x020000 <= EXPAT_VERSION
  , /* 2.0.0 */
  PAIR (XML_ERROR_RESERVED_PREFIX_XML),
  PAIR (XML_ERROR_RESERVED_PREFIX_XMLNS),
  PAIR (XML_ERROR_RESERVED_NAMESPACE_URI)
#endif  /* 2.0.0 */
#if 0x020201 <= EXPAT_VERSION
  , /* 2.2.1 */
  PAIR (XML_ERROR_INVALID_ARGUMENT)
#endif  /* 2.2.1 */
};

static int
  error_codes_count =
  sizeof (error_codes) / sizeof (struct enumsym);

/*
 * XML_Error
 */
static int
error_symbol_to_int (SCM sym)
{
  int code;

  for (code = 0; code < error_codes_count; code++)
    if (EQ (error_codes[code].sy, sym))
      return code;
  return -1;
}

PRIMPROC
(error_symbol, "error-symbol", 1, 0, 0,
 (SCM parser),
 doc: /***********
Return a symbol corresponding to the error code for @var{parser}.  */)
{
#define FUNC_NAME s_error_symbol
  XML_Parser p;
  enum XML_Error code;

  VALIDATE_PARSER ();

  return error_codes_count > (code = XML_GetErrorCode (p))
    ? error_codes[code].sy
    : SCM_BOOL_F;
#undef FUNC_NAME
}

PRIMPROC
(get_locus, "get-locus", 1, 1, 0,
 (SCM parser, SCM stash),
 doc: /***********
Return ``current'' locus information for @var{parser}
as a vector of four elements (all non-negative integers):

@example
#(LINE COLUMN BYTE-COUNT BYTE-INDEX)
@end example

Optional arg @var{stash} specifies a vector to fill in rather
than constructing a new one.  If an element in @var{stash} is
@code{#f}, the respective slot is skipped (it remains @code{#f}).  */)
{
#define FUNC_NAME s_get_locus
  XML_Parser p;
  SCM v;

  VALIDATE_PARSER ();
  UNBOUND_MEANS_FALSE (stash);
  if (NOT_FALSEP (stash))
    {
      SCM_VALIDATE_VECTOR (2, stash);
      v = stash;
    }
  else
    v = scm_make_vector (NUM_INT (4), SCM_UNSPECIFIED);

#define JAM(n,f)                                        \
  if (NOT_FALSEP (VECTOR_REF (v, n)))                   \
    scm_vector_set_x (v, NUM_INT (n), NUM_INT (f (p)))

  JAM (0, XML_GetCurrentLineNumber);
  JAM (1, XML_GetCurrentColumnNumber);
  JAM (2, XML_GetCurrentByteCount);
  JAM (3, XML_GetCurrentByteIndex);

#undef JAM

  return v;
#undef FUNC_NAME
}

PRIMPROC
(error_string, "error-string", 1, 0, 0,
 (SCM code),
 doc: /***********
Return a string representing the error @var{code} (a symbol).
If @var{code} is not recognized, return @code{#f}.  */)
{
#define FUNC_NAME s_error_string
  /* TODO: Handle multibyte XML_LChar.  */
  const XML_LChar *s;

  SCM_VALIDATE_SYMBOL (1, code);

  return (s = XML_ErrorString (error_symbol_to_int (code)))
    ? STRING (s)
    : SCM_BOOL_F;
#undef FUNC_NAME
}


/* Init.  */

static void
init_gexpat (void)
{
  int code;

  DEFSMOB (parser_tag, "XML-Parser",
           mark_parser, free_parser, print_parser);
  DEFSMOB (encoding_tag, "XML-Encoding",
           mark_encoding, free_encoding, print_encoding);

  sym_u64 = PERMANENT (SYMBOL ("u64"));

  halist = SCM_EOL;
  while (n_hnames--)
    halist = scm_acons (PERMANENT (SYMBOL (hnames[n_hnames])),
                        NUM_INT (n_hnames),
                        halist);
  halist = PERMANENT (halist);

  for (code = 0; code < param_entity_parsing_codes_count; code++)
    param_entity_parsing_codes[code].sy =
      PERMANENT (SYMBOL (param_entity_parsing_codes[code].nm));

  for (code = 0; code < error_codes_count; code++)
    error_codes[code].sy =
      PERMANENT (SYMBOL (error_codes[code].nm));

#define JAM(x)  statuses[x] = PERMANENT (SYMBOL (#x))
  JAM (XML_STATUS_ERROR);
  JAM (XML_STATUS_OK);
  JAM (XML_STATUS_SUSPENDED);
#undef JAM

#define JAM(x)  parsing_statuses[x] = PERMANENT (SYMBOL (#x))
  JAM (XML_INITIALIZED);
  JAM (XML_PARSING);
  JAM (XML_FINISHED);
  JAM (XML_SUSPENDED);
#undef JAM

#include "expat.x"
}

MOD_INIT_LINK_THUNK ("mixp expat"
                     ,mixp_expat
                     ,init_gexpat)

/*
 * Local variables:
 * coding: utf-8
 * End:
 */

/* expat.c ends here */
